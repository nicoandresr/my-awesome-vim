# My awesome vim

This is my awesome vim configuration, and my history in the vim way.

## Introduction

I like vim because, I like the console, I like work with commands, I like the cutomization, and I like the fast workflow, but the vim way not is easy, because you need learn the vim way first, then in the begining you can feel very lost and frustated,

## Prerequisites
my awesome neovim configuration uses vim-plug, check it out and install it from
`https://github.com/junegunn/vim-plug` IMPORTANT install vim-plug for NEOVIM.

## Install
For neovim just clone this repo on your local machine, on `.config/nvim/` folder whit

```shell
git clone https://gitlab.com/nicoandresr/my-awesome-vim.git ~/.config/nvim
```

if you're using just vim instead of neovim, you should link the templates to your vim folder

`mkdir ~/.vim/vim-templates`

`ln -s ~/.config/nvim/vim-templates/* ~/.vim/vim-templates/templates/`

## Postinstall for neovim
run `:checkhealt` inside neovim

