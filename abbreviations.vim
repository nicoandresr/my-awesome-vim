" ---------------------------- Abbreviations ------------------------------
" Expand Todo, Fixme and Info comments
iab <expr> f/ strftime('// FIXME: (' . $USER . ' %Y-%m-%d')
iab <expr> t/ strftime('// TODO: (' . $USER . ' %Y-%m-%d')
iab <expr> i/ strftime('// INFO: (' . $USER . ' %Y-%m-%d')
iab <expr> n/ strftime('// NOTE: (' . $USER . ' %Y-%m-%d')
