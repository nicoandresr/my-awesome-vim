"__________________________________________________________________________
" _\/\\\\\\___\/\\\_\/\\\_______\/\\\_\/////\\\///__\/\\\\\\________/\\\\\\_
"  _\/\\\/\\\__\/\\\_\//\\\______/\\\______\/\\\_____\/\\\//\\\____/\\\//\\\_
"   _\/\\\//\\\_\/\\\__\//\\\____/\\\_______\/\\\_____\/\\\\///\\\/\\\/_\/\\\_
"    _\/\\\\//\\\\/\\\___\//\\\__/\\\________\/\\\_____\/\\\__\///\\\/___\/\\\_
"     _\/\\\_\//\\\/\\\____\//\\\/\\\_________\/\\\_____\/\\\____\///_____\/\\\_
"      _\/\\\__\//\\\\\\_____\//\\\\\__________\/\\\_____\/\\\_____________\/\\\_
"       _\/\\\___\//\\\\\______\//\\\________/\\\\\\\\\\\_\/\\\_____________\/\\\_
"        _\///_____\/////________\///________\///////////__\///______________\///__
"         __________________________________________________________________________
" ------------------------------ AutoConfig ( VimPlug ) ------------------------------
" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif
" ------------------------------ Plugins ( VimPlug ) ------------------------------
call plug#begin('~/.vim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'} " Make your Neovim as smart as VSCode.
Plug 'sheerun/vim-polyglot' " A collection of language packs for Vim.
Plug 'ap/vim-css-color' " Context-sensitive color name highlighter.
Plug 'tpope/vim-fugitive' " Fugitive is the premier Vim plugin for Git.
Plug 'SirVer/ultisnips' " The ultimate solution for snippets in Vim.
Plug 'vimwiki/vimwiki' " Personal wiki.
Plug 'aperezdc/vim-template' " Simple Vim temlpates plugin.
Plug 'tpope/vim-surround' " Quoting/parenthesizing made simple.
Plug 'heavenshell/vim-jsdoc' " Generate JSDoc to your JavaScript code.
Plug 'junegunn/goyo.vim' " Distraction-free writing in Vim.
Plug 'junegunn/limelight.vim' " Hyperfocus-writing in Vim.
Plug 'skywind3000/asyncrun.vim' " Run shell commands in background.
Plug 'skywind3000/vim-terminal-help' " Small changes make vim/nvim's internal terminal great again !!.
Plug 'edkolev/tmuxline.vim' " Simple tmux statusline generator.
Plug 'andreshazard/vim-freemarker' " Freemarker (ftl) syntax and functions.
Plug 'preservim/nerdcommenter' " Comment functions so powerful—no comment necessary.
Plug 'nvim-lua/plenary.nvim' " All the lua functions I don't want to write twice.
Plug 'nvim-telescope/telescope.nvim' " Find, Filter, Preview, Pick. All lua, all the time.
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " Treesitter configurations and abstraction layer 
Plug 'fannheyward/telescope-coc.nvim' " coc.nvim integration for telescope.nvim 
Plug 'folke/todo-comments.nvim' " ✅ Highlight, list and search todo comments in your projects.
Plug 'kyazdani42/nvim-web-devicons' " lua `fork` of vim-web-devicons for neovim.
Plug 'henrik/rename.vim' " Rename the current file in the vim buffer + retain relative path.
Plug 'alvan/vim-closetag' " Auto close (X)HTML tags.
Plug 'aquach/vim-http-client' " Forget your curl today! Make HTTP requests from Vim without wrestling the command line!
Plug 'nvim-tree/nvim-web-devicons' " lua `fork` of vim-web-devicons for neovim.
Plug 'AlphaTechnolog/pywal.nvim', { 'as': 'pywal' } " dynamic color scheme via pywal.
Plug 'nvim-lualine/lualine.nvim' " A blazing fast and easy to configure neovim statusline.
Plug 'kyazdani42/nvim-web-devicons' " lua `fork` of vim-web-devicons for neovim.
Plug 'akinsho/bufferline.nvim', { 'tag': 'v3.*' } " A snazzy bufferline for Neovim.
Plug 'codota/tabnine-nvim', { 'do': './dl_binaries.sh' } " Tabnine AI helper. 
Plug 'folke/trouble.nvim' " 🚦 A pretty diagnostics, references, telescope results, quickfix and location
Plug 'nvim-neotest/neotest' " An extensible framework for interacting with tests within NeoVim.
Plug 'nvim-neotest/neotest-python' " Neotest adapter for python. Supports Pytest and unittest test files.
Plug 'lewis6991/gitsigns.nvim' " Git integration for buffers.
Plug 'sindrets/diffview.nvim' " Single tabpage interface for easily cycling through diffs for all modified files for any git rev.
" Plug 'folke/neodev.nvim' " 💻 Neovim setup for init.lua and plugin development with full signature help, docs and completion for the nvim lua API.
Plug 'mfussenegger/nvim-dap' " Debug Adapter Protocol client implementation for Neovim.
call plug#end()

" ---------------------------- General Settings -----------------------------
source ~/.config/nvim/general_settings.vim
" ----------------------------- Key Bindings --------------------------------
source ~/.config/nvim/key_bindings.vim
" ----------------------------- Project Settings ----------------------------
source ~/.config/nvim/projects_settings.vim
" ----------------------------- CoC Settings --------------------------------
source ~/.config/nvim/coc_settings.vim
" ---------------------------- Plugin Settings ------------------------------
source ~/.config/nvim/plugin_settings.vim
" ---------------------------- Abbreviations --------------------------------
source ~/.config/nvim/abbreviations.vim
