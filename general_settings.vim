" ---------------------------- General Settings -----------------------------
set autoindent
set cursorline
set expandtab
set number
set shiftwidth=2
set softtabstop=2
set tabstop=2
set termguicolors
" Python executable
let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'
" No highlight seearch ocurrences
nnoremap ,<space> :nohlsearch<CR>
" Remap leader
let mapleader = ","
" ---------- Leader shortcuts ----------
" ,n next buffer
map <leader>n :bn<CR>
" ,p previous buffer
map <leader>p :bp<CR>
" ,d delete buffer
map <leader>d :bd<CR>
" ,t jest test for current file
map <leader>t :AsyncRun -mode=term -pos=thelp jest %<CR>
" ,h toggle vim-terminal-help
map <leader>h <m-=>
" ,x save an closing file
map <leader>x :x<CR>
" ,w save buffer
map <leader>w :w<CR>
" ,e open in current location
map <leader>e :e %:h
" ,jsd run JSDoc
map <leader>jsd :JsDoc<CR>
" ,s run UltiSnipsEdit
map <leader>u :UltiSnipsEdit<CR>
" ---------- Enable Italics ------------
highlight Comment cterm=italic
" --- Opens splits to right/below ------
set splitbelow
set splitright
" ---------- System clipboard ----------
set clipboard+=unnamedplus
" ---------- Color column 81 -----------
set colorcolumn=81
