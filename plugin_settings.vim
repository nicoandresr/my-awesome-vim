" ---------------------------- Plugin Settings ------------------------------
" UltiSnips
let g:UltiSnipsExpandTrigger="<c-s>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"
" Ale
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['prettier', 'eslint'],
\}
let g:ale_fix_on_save = 0
let g:ale_sign_error = '🚧'
let g:ale_sign_warning = '🐞'
let g:ale_set_highlights = 0
let g:ale_sign_column_always = 1
let g:ale_echo_msg_error_str = '👮'
let g:ale_echo_msg_warning_str = '💅'
let g:ale_echo_msg_format = '%severity% %s %severity%'
" Vim-Fugitive
command Gp Git pull
command Gb Git branch
command Gsl Git stash list
command Gsp Git stash pop
command -nargs=1 Gc Git checkout "<args>"
command -nargs=1 Gcb Git checkout -b "<args>"
command -nargs=1 Gcm Git commit -am "<args>"
command -nargs=1 Gbd Git branch -D "<args>"
" ---------- Goyo ----------
nnoremap <silent><C-g> :Goyo<CR>
function! s:goyo_enter()
  silent !tmux set status off
endfunction
function! s:goyo_leave()
  silent !tmux set status on
endfunction
" Limelight
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
" AsyncRun
let g:asyncrun_open = 6
" ---------- NerdCommenter ----------
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" ---------- to-do comments ---------- 
lua << EOF
  require('todo-comments').setup {
    -- your configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below
  }
EOF
" -------------- Lehre ---------------
let g:jsdoc_lehre_path = '/usr/local/bin/lehre'
" ------------- UltiSnips ------------
let g:UltiSnipsExpandTrigger='<C-l>'
" ------------- Telescope ------------
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fd <cmd>Telescope cocdiagnostics<cr>
nnoremap <leader>fp <cmd>Telescope git_files<cr>
" ------------- Telescope COC --------
lua << EOF
require("telescope").setup({
  extensions = {
    coc = {
        theme = 'ivy',
        prefer_locations = true, -- always use Telescope locations to preview definitions/declarations/implementations etc
    }
  },
})
require('telescope').load_extension('coc')
EOF

" Pywall
colorscheme pywal

" Bufferline
set termguicolors
lua << EOF
require('bufferline').setup{}
EOF

" Tabnine "
lua << EOF
require('tabnine').setup({
  disable_auto_comment=true,
  accept_keymap='<Tab>',
  dismiss_keymap = '<C-]>',
  debounce_ms = 300,
  suggestion_color = {gui = "#808080", cterm = 244},
  execlude_filetypes = {"TelescopePrompt"}
})
EOF

" LuaLine "
lua << END
require('lualine').setup({
  tabline = {
      lualine_a = {},
      lualine_b = {'branch'},
      lualine_c = {'filename'},
      lualine_x = {},
      lualine_y = {},
      lualine_z = {}
  },
  sections = {lualine_c = {'lsp_progress'}, lualine_x = {'tabnine'}}
})
END
" === NeoTest === "
lua << END
require("neotest").setup({
  diagnostic = {
    enabled = true
  },
  adapters = {
    require("neotest-python")({
        -- Extra arguments for nvim-dap configuration
        -- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
        dap = { justMyCode = false },
        -- Command line arguments for runner
        -- Can also be a function to return dynamic values
        args = {"--log-level", "DEBUG"},
        -- Runner to use. Will use pytest if available by default.
        -- Can be a function to return dynamic value.
        runner = "pytest",
    }),
  },

  highlights = {
    passed = "NeotestPassed",
    running = "NeotestRunning",
    failed = "NeotestFailed",
    skipped = "NeotestSkipped",
    test = "NeotestTest",
    namespace = "NeotestNamespace",
    focused = "NeotestFocused",
    file = "NeotestFile",
    dir = "NeotestDir",
    border = "NeotestBorder",
    indent = "NeotestIndent",
    expand_marker = "NeotestExpandMarker",
    adapter_name = "NeotestAdapterName",
    select_win = "NeotestWinSelect",
    marked = "NeotestMarked",
  },

  icons = {
    expanded = "",
    collapsed = "",
    passed = "✅",
    running = "✨",
    failed = "❌",
    unknown = "❔",
  },
})
END
let test#python#pytest#options = "--color=yes"
command NTestRun :lua require("neotest").run.run(vim.fn.expand("%"))
command NTestRunNearest :lua require("neotest").run.run()
command NTestStopNearest :lua require("neotest").run.stop()
command NTestSummaryOpen :lua require("neotest").summary.open()
command NTestSummaryClose :lua require("neotest").summary.close()
command NTestSummaryToggle :lua require("neotest").summary.toggle()
command NTestOutput :lua require("neotest").output.open({ enter = true })
" === Gitsigns  === "
lua << END
require('gitsigns').setup()
END
