function! SetupEnvironment()
  let l:path = expand('%:p')
  if l:path =~ 'home/veda/hm/dls'
    " eslint correct config path
    let g:ale_javascript_eslint_options = '-c conf/.eslintrc.json'
  endif
  if l:path =~ 'home/veda/hm/api'
    " eslint correct config path
    let g:ale_javascript_eslint_options = '-c config/.eslintrc.json'
  endif
  if l:path =~ 'home/Projects/gitlab/calc'
    " FZF
    nnoremap <C-p> :Files<CR>
  endif
  " ------------------- Config for Investor Services ---------------------------
  if l:path =~ '/Users/nicolasrodriguez/js/is'
    " FZF
    nnoremap <C-p> :GFiles<CR>
  endif
endfunction

autocmd! BufReadPost,BufNewFile,VimEnter * call SetupEnvironment()
