" ----------------------------- Mappings --------------------------------
" hh to Esc.
inoremap hh <Esc>
vnoremap <Space> <Esc>
" m to next/previous occurrence.
nnoremap M N
nnoremap m n
" Dvorak motions.
nnoremap j h
nnoremap t j
nnoremap n k
nnoremap s l
vnoremap j h
vnoremap t j
vnoremap n k
vnoremap s l
" Move selected blocks.
xnoremap N :move '<-2<CR>gv-gv
xnoremap T :move '>+1<CR>gv-gv
" Smart way to move between panes
" map <up> <C-w><up>
" map <down> <C-w><down>
" map <left> <C-w><left>
" map <right> <C-w><right>
" Save with Ctrl-s
nnoremap <C-s> :w<CR>
inoremap <C-s> <Esc>:w<CR>
vnoremap <C-s> :sort <CR>
" Panel motion.
" nnoremap <Tab> :bn! <CR>
" nnoremap <S-Tab> :bp! <CR>
" Automaticc closing brackets
inoremap " ""<left>
inoremap ' ''<left>
inoremap ` ``<left>
inoremap ( ()<left>
inoremap [ []<left>
" inoremap < <><left>
inoremap { {}<left>
inoremap ${ ${}<left>
inoremap {<CR> {<CR>}<ESC>0
" Double space over word to find and replace.
nnoremap <Space><Space> :%s/\<<C-r>=expand("<cword>")<CR>\>/
" Create automatic dicrectories that does not exists
augroup Mkdir
  autocmd!
  autocmd BufWritePre * call mkdir(expand("<afile>:p:h"), "p")
augroup END
" Easy copy
" vnoremap <C-c> :'<,'>w !xclip -sel c<CR><CR>
" nnoremap <C-a> :%w !xclip -sel c<CR><CR>
